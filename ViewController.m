//
//  ViewController.m
//  AudioQueueServicesExample
//
//  Created by Shivansh on 6/12/17.
//
//

#import "ViewController.h"
#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#define kDestinationURL @"http://192.168.43.138:8081/v1/iphone/audio/info/"

@interface ViewController ()
{ AVAudioSession *session;
}

@end



@implementation ViewController 

@synthesize appDelegate; //global declaration bitches
@synthesize TextView;

@synthesize shorts = _shorts;
@synthesize decoded = _decoded;
@synthesize trigger = _trigger;

+ (ViewController *)sharedInstance {
    static dispatch_once_t onceToken;
    static ViewController *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[ViewController alloc] init];
    });
    return instance;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //This ensures it plays properly on speaker output
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord
                                     withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker
                                           error:nil];
    
     appDelegate = (AudioRecorderAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //Text Properties
    self.TextView.backgroundColor = [UIColor whiteColor];   //Without setting this, text was overwritten.
    self.TextView.text=@"OTP";
    
    _shorts = nil;
    _decoded = nil;
    _trigger = nil;
   
}


- (IBAction)startrec:(UIButton *)sender {
    [appDelegate startRecording];
}


- (IBAction)stoprec:(UIButton *)sender {
    [appDelegate stopRecording];
}

- (IBAction)playrec:(UIButton *)sender {
    [appDelegate startPlayback];
}

- (void)storeToShorts: (NSString *)allBuffersString : (NSString *) pair_result :(NSString *) trig_result
{   ViewController *globals = [ViewController sharedInstance];
    globals.shorts = allBuffersString ;
    globals.decoded = pair_result;
    globals.trigger = trig_result;

}





- (IBAction)sendServer:(UIButton *)sender {
    ViewController *globals = [ViewController sharedInstance];
    
    NSURL *icyURL = [NSURL URLWithString:kDestinationURL];
    NSString *vecArray = [NSString stringWithFormat: @"%@",globals.shorts];
    NSString *decArray = [NSString stringWithFormat: @"%@",globals.decoded];
    NSString *trigArray = [NSString stringWithFormat: @"%@",globals.trigger];
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:vecArray forKey:@"vec"];
    [dict setValue:decArray forKey:@"decoded"];
    [dict setValue:trigArray forKey:@"trigger"];
    //    NSData *authData = [vecArray dataUsingEncoding:NSUTF8StringEncoding];
    //    NSString *authHeader = [NSString stringWithFormat: @"Basic %@",
    //                            [authData base64EncodedStringWithOptions:0]];
    
    
    //    NSArray *jsonObject = [NSJSONSerialization JSONObjectWithData:[vecArray dataUsingEncoding:NSUTF8StringEncoding]
    //                                                          options:0 error:NULL]
    NSData *requestData = [NSData dataWithBytes:[vecArray UTF8String] length:[vecArray lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
    requestData =   [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:icyURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody: requestData];
    NSLog(@"\nRequest : %@", request);
    NSLog(@"\nNSDATA : %@", [[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session =
    [NSURLSession sessionWithConfiguration:sessionConfig delegate: (id)self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *_Nullable data, NSURLResponse *_Nullable response, NSError *_Nullable error) {
                                      if (error) {
                                          // do something with the erroE
                                          return;
                                      }
                                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                      if (httpResponse.statusCode == 201) {
                                          NSArray* arrTokenData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                          NSLog(@"%@", arrTokenData);
                                          //[self postDomain];
                                      } else {
                                          // failure: do something else on failure
                                          NSLog(@"httpResponse code: %@", [NSString stringWithFormat:@"%ld", (unsigned long)httpResponse.statusCode]);
                                          NSLog(@"httpResponse head: %@", httpResponse.allHeaderFields);
                                          
                                          return;
                                      }
                                  }];
    [task resume];
}


//- (void)dealloc {
//    [UITextView release];
//    [super dealloc];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
