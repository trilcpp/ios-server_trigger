//
//  ViewController.h
//  AudioQueueServicesExample
//
//  Created by Shivansh on 6/12/17.
//
//

#import <UIKit/UIKit.h>
#import "AudioRecorderAppDelegate.h"
#import <AVFoundation/AVFoundation.h>



@interface ViewController : UIViewController <AVAudioSessionDelegate>
{
    NSString *_shorts;
    NSString *_decoded;
    NSString *_trigger;
}

//Our functions

@property (nonatomic, strong) AudioRecorderAppDelegate *appDelegate; //Global array declaration
@property(strong, nonatomic, readwrite) NSString *shorts;
@property(strong, nonatomic, readwrite) NSString *decoded;
@property(strong, nonatomic, readwrite) NSString *trigger;

+ (ViewController *) sharedInstance;
- (void)storeToShorts: (NSString *)allBuffersString : (NSString *)pair_result :(NSString *) trig_result;



//Buttons
- (IBAction)startrec:(UIButton *)sender;
- (IBAction)stoprec:(UIButton *)sender;
- (IBAction)playrec:(UIButton *)sender;
- (IBAction)sendServer:(UIButton *)sender;

@property (retain, nonatomic) IBOutlet UILabel *TextView;



@end
