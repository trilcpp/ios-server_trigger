//
//  AudioRecorderAppDelegate.h
//  AudioRecorder
//
//  Copyright Shivansh Jagga. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/Audiotoolbox.h>
#import <AudioToolbox/AudioQueue.h>
#import <AudioToolbox/AudioFile.h>




//#### AUDIO FORMAT SETTINGS IN .pch file ###

// Struct defining recording state
typedef struct
{
    AudioStreamBasicDescription  dataFormat;
    AudioQueueRef                queue;
    AudioQueueBufferRef          buffers[NUM_BUFFERS];
    AudioFileID                  audioFile;
    UInt32						 mNumPacketsToRead;
    SInt64                       currentPacket;
    bool                         recording;    
} RecordState;

// Struct defining playback state
typedef struct
{
    AudioStreamBasicDescription  dataFormat;
    AudioQueueRef                queue; //The playback audio queue created by your application.
    AudioQueueBufferRef          buffers[NUM_BUFFERS];  //An array holding pointers to the audio queue buffers managed by the audio queue.
    AudioFileID                  audioFile;     //An audio file object that represents the audio file your program plays.
    UInt32						 mNumPacketsToRead; //STOLEN :The number of packets to read on each invocation of the audio queue’s playback callback
    SInt64                       currentPacket; //The packet index for the next packet to play from the audio file.
    bool                         playing;
} PlayState;

// The main application
@interface AudioRecorderAppDelegate : NSObject  <UIApplicationDelegate>
{
    
   //UIViewController *viewcontroller;
    
    UIWindow *window;
    UILabel* labelStatus;
    UIButton* buttonRecord;
    UIButton* buttonPlay;
	RecordState recordState;
    PlayState playState;
    CFURLRef fileURL;
    
    //TPCircularBuffer c_buffer;      //For buffers of circular queue we define this global object
}

@property (nonatomic, retain) UIWindow *window;
//trill, for accessing ui lavel
//@property (nonatomic, retain) UIViewController *viewcontroller;

- (BOOL)getFilename:(char*)buffer maxLenth:(int)maxBufferLength;
- (void)setupAudioFormat:(AudioStreamBasicDescription*)format;
- (void)recordPressed:(id)sender;
- (void)playPressed:(id)sender;
- (void)startRecording;
- (void)stopRecording;
- (void)startPlayback;
- (void)stopPlayback;

- (void)feedSamplesToEngine:(UInt32)audioDataBytesCapacity audioData:(void *)audioData;



/////////////////////
@end
