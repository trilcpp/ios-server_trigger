//
//  CompareData.cpp
//  Server_trigger
//
//  Created by Shivansh on 9/9/17.
//
//

#include "CompareFindData.hpp"
#include "correlate.hpp"
#include <itpp/base/sort.h>

#define MIN_DIS_BWT_PEAKS 1206
#define MAX_DIS_BWT_PEAKS 10000
#define PEAK_FACTOR 23
int INDICES_FOR_PEAKS = 5;  //mAKE it 30 for stringbuffer&NewBuffer, else 20 for shortbuffer

//Parameters for New algo
#define FRAME_SIZE 1206
#define BACK_SECURE_FRAME 1
#define PERCENTAGE 45
#define EXPECTED_PEAK_PAIR 4
// @MIN_DIS_BWT_PEAKS -> Minimum size after which the next peak start is expected
// @PERCENTAGE -> percentage of biggest peak after which a new peak start is expected
// @SEPARATION_BW_PAIROFPEAKS -> Separation after which a pair of peaks occurs
// @EXPECTED_PEAK_PAIR -> Total pair of peaks we'll get such that after @SEPARATION_BW_PAIROFPEAKS*@EXPECTED_PEAK_PAIR we get nothing

float FindData::biggest_peak(vec corr_op, const int &size){
    float big = corr_op[0];
    
    F(i,0,size) {
        if (corr_op[i] > big)
            big = corr_op[i];
    }
    big = big*(PERCENTAGE/100.0); //Anything above 45% Peak, make sure its 100.0
    return big;
}
vector<int> FindData::mark_every_frame(vec corr_op, const int &size){
    float big;
    vector<int> marked_array;
    big = biggest_peak(corr_op, size);
    F(i,0,size)
    if(corr_op[i] >= big){
        marked_array.push_back(i);
        i += FRAME_SIZE;
    }
    return marked_array;
}

std::pair<int, int> FindData::findDataPtFromDistBwPeaks_sort(vec corrOut, const int &size, int top_peaks) {
    INDICES_FOR_PEAKS = top_peaks;
    vector<int> marked_array;
    marked_array = mark_every_frame(corrOut,size);
    int no_of_frames = int( marked_array.size() ); //Total marked points(having peak data) in the whole sample
    if(no_of_frames > EXPECTED_PEAK_PAIR)
        return make_pair(-1,-(no_of_frames));
    cout<<"\nMARKED ARRAY: ";
    F(i,0,no_of_frames){
        cout<<marked_array[i]<<" ";
    }
    cout<<endl;
    
    ivec maxIndices;
    int max_size;
//@sorted_mag : contains the magnitude of the top @INDICES_FOR_PEAKS
//@sorted_marked_index : contains the 'ORIGINAL' index of the top @INDICES_FOR_PEAKS
    vec sorted_mag;
    ivec sorted_marked_index;
    F(i,0,no_of_frames){
        //#Getting new array of size FRAME_SIZE + BACK_SECURE_FRAME
        int framesize_macro;
        if( (marked_array[i] + FRAME_SIZE) > size)
            framesize_macro = -(marked_array[i]) + (size-1);
        else framesize_macro = FRAME_SIZE;
        
        vec new_array = corrOut.get(marked_array[i]-BACK_SECURE_FRAME, marked_array[i] + framesize_macro);
        int resIndexLen = marked_array[i] + framesize_macro - (marked_array[i]-BACK_SECURE_FRAME);
        if(resIndexLen<11)
            continue;
        
        ivec resIndex = sort_index(new_array);
        resIndex = resIndex.get(resIndexLen-INDICES_FOR_PEAKS, resIndexLen-1);
        F(j,0,INDICES_FOR_PEAKS){
            sorted_mag = concat(sorted_mag, new_array[ resIndex[j] ]);  //Magnitude of the sorted peaks
            resIndex[j] = resIndex[j] + (marked_array[i]- BACK_SECURE_FRAME); //Adding @marked_array[], to get original index
        }
        sorted_marked_index = concat(sorted_marked_index, resIndex); //Contains the new resiNDEX with original peaks
    }

//@sorted_mag_index : Containes indexes of the magnitude after sorting
    ivec sorted_mag_index;
    sorted_mag_index = sort_index(sorted_mag);
    max_size = sorted_mag.size();
    cout<<"++";
//After sorting magnitude index, putting the ORIGINAL index values of where that magnitude occurs
    F(i,0,max_size){
        maxIndices = concat(maxIndices, sorted_marked_index[ sorted_mag_index[max_size -1- i] ]);
        cout<<maxIndices[i]<<" ";
    }
    
    //cout<<"&&"<<maxIndices.size();
    //max_size = (no_of_frames)*FRAME_SIZE;
    //  If Multiple peak data,i.e,instead of two multiple peaks are detected; still select the only top 4*INDICES_FOR_PEAKS peaks
//    if(no_of_frames > 4){
//        itpp::sort(maxIndices);
//        maxIndices =   (max_size- 4*INDICES_FOR_PEAKS, max_size - 1);
//        max_size = 4*INDICES_FOR_PEAKS;
//    }
    
    //    cout<<"\nPeaks store in maxIndices: ";
    //    F(i,0,max_size){
    //        cout<<maxIndices[i]<<" ";
    //    }
    cout<<"-----------------------------------------";
    int peak1Selected;
    int peak2Selected;
    for (int i=0;i< max_size; i++) {
        peak1Selected = maxIndices[i];
        //        INDICES_FOR_PEAKS* floor((i+INDICES_FOR_PEAKS)/INDICES_FOR_PEAKS)
        for (int j= 0 ; j < max_size; j++) {
            peak2Selected = maxIndices[j];
            int absDistance = abs(peak1Selected - peak2Selected);
            printf("Abs ij %d %d::%d ",i,j,absDistance);
            if (absDistance > MIN_DIS_BWT_PEAKS && absDistance < MAX_DIS_BWT_PEAKS) {
                int FinalDistance = (absDistance - MIN_DIS_BWT_PEAKS );
                if ( FinalDistance % PEAK_FACTOR < 1) {
                    FinalDistance = FinalDistance - (FinalDistance % PEAK_FACTOR);
                    if (peak1Selected > peak2Selected) {
                        return make_pair(FinalDistance + MIN_DIS_BWT_PEAKS,(i)*INDICES_FOR_PEAKS + (j+1));
                    }
                    return make_pair(FinalDistance + MIN_DIS_BWT_PEAKS , (i)*INDICES_FOR_PEAKS + (j+1));
                }
                else if (FinalDistance % PEAK_FACTOR > 22) {
                    FinalDistance = FinalDistance + (PEAK_FACTOR - (FinalDistance % PEAK_FACTOR));
                    if (peak1Selected > peak2Selected) {
                        return make_pair(FinalDistance + MIN_DIS_BWT_PEAKS, (i)*INDICES_FOR_PEAKS + (j+1));
                    }
                    return make_pair(FinalDistance + MIN_DIS_BWT_PEAKS, (i)*INDICES_FOR_PEAKS + (j+1));
                }
            }
        }
    }
    cout<<endl;
    return make_pair(-1,-no_of_frames);
}


std::pair<int, int> FindData::apply(vec inputWave, int algorithm) {
    vec syncWave = "0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0 0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0 0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0 0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0 0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0 0.0 0.0 0.0589124263711 -0.0797570710988 0.0348754823696 0.076320485321 -0.197602565214 0.242694813812 -0.150358902304 -0.0654675764475 0.305687039798 -0.430352962901 0.338229762807 -0.0370390433835 -0.34095001351 0.595965997136 -0.566556603363 0.227943042802 0.275703205853 -0.691557899905 0.786589272086 -0.478133473282 -0.106696705851 0.682078313751 -0.945437986304 0.738681855154 -0.141716505281 -0.55661361325 1.0008755661 -0.953253022038 0.422515589168 0.333022911221 -0.933757208014 1.07332585023 -0.677859452953 -0.0543157478769 0.75415840529 -1.07165574977 0.854435539366 -0.22234618153 -0.499218821708 0.949865328499 -0.917733564077 0.440814381481 0.223246024687 -0.737835039394 0.860960727426 -0.560758202688 0.0169588111715 0.485106604284 -0.705978761484 0.567706598027 -0.177129495801 -0.246978665665 0.49613389417 -0.475450815616 0.236452038228 0.0695628175222 -0.283351003548 0.320348826868 -0.201281010824 0.0217269575244 0.113559860318 -0.149580600981 0.100888057351 -0.0269975618291 0.0 0.0 0.0 0.0589124263711 -0.0797570710988 0.0348754823696 0.076320485321 -0.197602565214 0.242694813812 -0.150358902304 -0.0654675764475 0.305687039798 -0.430352962901 0.338229762807 -0.0370390433835 -0.34095001351 0.595965997136 -0.566556603363 0.227943042802 0.275703205853 -0.691557899905 0.786589272086 -0.478133473282 -0.106696705851 0.682078313751 -0.945437986304 0.738681855154 -0.141716505281 -0.55661361325 1.0008755661 -0.953253022038 0.422515589168 0.333022911221 -0.933757208014 1.07332585023 -0.677859452953 -0.0543157478769 0.75415840529 -1.07165574977 0.854435539366 -0.22234618153 -0.499218821708 0.949865328499 -0.917733564077 0.440814381481 0.223246024687 -0.737835039394 0.860960727426 -0.560758202688 0.0169588111715 0.485106604284 -0.705978761484 0.567706598027 -0.177129495801 -0.246978665665 0.49613389417 -0.475450815616 0.236452038228 0.0695628175222 -0.283351003548 0.320348826868 -0.201281010824 0.0217269575244 0.113559860318 -0.149580600981 0.100888057351 -0.0269975618291 0.0 0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0 0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0 0.0 0.0 0.0589124263711 -0.0797570710988 0.0348754823696 0.076320485321 -0.197602565214 0.242694813812 -0.150358902304 -0.0654675764475 0.305687039798 -0.430352962901 0.338229762807 -0.0370390433835 -0.34095001351 0.595965997136 -0.566556603363 0.227943042802 0.275703205853 -0.691557899905 0.786589272086 -0.478133473282 -0.106696705851 0.682078313751 -0.945437986304 0.738681855154 -0.141716505281 -0.55661361325 1.0008755661 -0.953253022038 0.422515589168 0.333022911221 -0.933757208014 1.07332585023 -0.677859452953 -0.0543157478769 0.75415840529 -1.07165574977 0.854435539366 -0.22234618153 -0.499218821708 0.949865328499 -0.917733564077 0.440814381481 0.223246024687 -0.737835039394 0.860960727426 -0.560758202688 0.0169588111715 0.485106604284 -0.705978761484 0.567706598027 -0.177129495801 -0.246978665665 0.49613389417 -0.475450815616 0.236452038228 0.0695628175222 -0.283351003548 0.320348826868 -0.201281010824 0.0217269575244 0.113559860318 -0.149580600981 0.100888057351 -0.0269975618291 0.0 0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0 0.0 0.0 0.0589124263711 -0.0797570710988 0.0348754823696 0.076320485321 -0.197602565214 0.242694813812 -0.150358902304 -0.0654675764475 0.305687039798 -0.430352962901 0.338229762807 -0.0370390433835 -0.34095001351 0.595965997136 -0.566556603363 0.227943042802 0.275703205853 -0.691557899905 0.786589272086 -0.478133473282 -0.106696705851 0.682078313751 -0.945437986304 0.738681855154 -0.141716505281 -0.55661361325 1.0008755661 -0.953253022038 0.422515589168 0.333022911221 -0.933757208014 1.07332585023 -0.677859452953 -0.0543157478769 0.75415840529 -1.07165574977 0.854435539366 -0.22234618153 -0.499218821708 0.949865328499 -0.917733564077 0.440814381481 0.223246024687 -0.737835039394 0.860960727426 -0.560758202688 0.0169588111715 0.485106604284 -0.705978761484 0.567706598027 -0.177129495801 -0.246978665665 0.49613389417 -0.475450815616 0.236452038228 0.0695628175222 -0.283351003548 0.320348826868 -0.201281010824 0.0217269575244 0.113559860318 -0.149580600981 0.100888057351 -0.0269975618291 0.0 0.0 0.0 -0.0589124263711 0.0797570710988 -0.0348754823696 -0.076320485321 0.197602565214 -0.242694813812 0.150358902304 0.0654675764475 -0.305687039798 0.430352962901 -0.338229762807 0.0370390433835 0.34095001351 -0.595965997136 0.566556603363 -0.227943042802 -0.275703205853 0.691557899905 -0.786589272086 0.478133473282 0.106696705851 -0.682078313751 0.945437986304 -0.738681855154 0.141716505281 0.55661361325 -1.0008755661 0.953253022038 -0.422515589168 -0.333022911221 0.933757208014 -1.07332585023 0.677859452953 0.0543157478769 -0.75415840529 1.07165574977 -0.854435539366 0.22234618153 0.499218821708 -0.949865328499 0.917733564077 -0.440814381481 -0.223246024687 0.737835039394 -0.860960727426 0.560758202688 -0.0169588111715 -0.485106604284 0.705978761484 -0.567706598027 0.177129495801 0.246978665665 -0.49613389417 0.475450815616 -0.236452038228 -0.0695628175222 0.283351003548 -0.320348826868 0.201281010824 -0.0217269575244 -0.113559860318 0.149580600981 -0.100888057351 0.0269975618291 0.0";

 
//##########################################################################################
#pragma mark - Apply
//------------------------------------------------------------------------------

    Correlate corr;
    vec resCorr = corr.apply(inputWave, syncWave);
    const int size = resCorr.size();
    
    
    
    std::pair<int, int> result;
//Respectively: 3->top20 peak algo ; 2->sort10 ; 1->sort5
    if(algorithm==1)
        result =  findDataPtFromDistBwPeaks_top20(resCorr, size);
    else if(algorithm==2)
        result =  findDataPtFromDistBwPeaks_sort(resCorr, size, 5);
    else if(algorithm==3)
        result =  findDataPtFromDistBwPeaks_sort(resCorr, size, 6);
    else if(algorithm==4)
        result =  findDataPtFromDistBwPeaks_sort(resCorr, size, 7);
    else if(algorithm==5)
        result =  findDataPtFromDistBwPeaks_sort(resCorr, size, 8);
    else
        result =  findDataPtFromDistBwPeaks_sort(resCorr, size, 10);
    return result;
}


std::pair<int, int> FindData::findDataPtFromDistBwPeaks_top20(vec corrOut,const int &size){
    INDICES_FOR_PEAKS = 20;
    //## ......short buffer
    ivec resIndex = sort_index(corrOut);
    int resIndexLen = resIndex.size();
    ivec maxIndices = resIndex.get(resIndexLen-INDICES_FOR_PEAKS, resIndexLen-1);
    
    int i, j;
    
    for (i=0;i< INDICES_FOR_PEAKS; i++) {
        for (j=0;j < INDICES_FOR_PEAKS; j++) {
            int absDistance = abs(maxIndices[INDICES_FOR_PEAKS-1-i] - maxIndices[INDICES_FOR_PEAKS-1-j]);
            if (absDistance > MIN_DIS_BWT_PEAKS && absDistance < MAX_DIS_BWT_PEAKS) {
                //###Statement for deviation
                int FinalDistance = (absDistance - MIN_DIS_BWT_PEAKS );
                if ( FinalDistance % PEAK_FACTOR < 2) {
                    FinalDistance = FinalDistance - (FinalDistance % PEAK_FACTOR);
                    if (maxIndices[INDICES_FOR_PEAKS-1-i] > maxIndices[INDICES_FOR_PEAKS-1-j]) {
                        return make_pair(FinalDistance + MIN_DIS_BWT_PEAKS, (i)*INDICES_FOR_PEAKS + (j+1));
                    }
                    return make_pair(FinalDistance + MIN_DIS_BWT_PEAKS , (i)*INDICES_FOR_PEAKS + (j+1));
                }
                if (FinalDistance % PEAK_FACTOR > 21) {
                    FinalDistance = FinalDistance + (PEAK_FACTOR - (FinalDistance % PEAK_FACTOR));
                    if (maxIndices[INDICES_FOR_PEAKS-1-i] > maxIndices[INDICES_FOR_PEAKS-1-j]) {
                        return make_pair(FinalDistance + MIN_DIS_BWT_PEAKS, (i)*INDICES_FOR_PEAKS + (j+1));
                    }
                    return make_pair(FinalDistance + MIN_DIS_BWT_PEAKS, (i)*INDICES_FOR_PEAKS +(j+1));
                }
            }
        }
    }
    
    return make_pair(-1,-1);
}


int* FindData::findDataPtFromDistBwPeaks_v3(vec corrOut, int *result_array) {
    
    const int size = corrOut.size();
    vector<int> marked_array;
    marked_array = mark_every_frame(corrOut,size);
    int no_of_frames = int( marked_array.size() ); //Total marked points(having peak data) in the whole sample
    if(no_of_frames > EXPECTED_PEAK_PAIR){
        F(i,0,3)
        result_array[i] = -1;
        return result_array;
    }
    cout<<"\nMARKED ARRAY: ";
    F(i,0,no_of_frames){
        cout<<marked_array[i]<<" ";
    }
    cout<<endl;
    int FINAL_ARRAY_SIZE = INDICES_FOR_PEAKS*no_of_frames;
    //@sorted_mag : contains the magnitude of the top @INDICES_FOR_PEAKS
    //@sorted_marked_index[][]: contains the 'ORIGINAL' index of the top @INDICES_FOR_PEAKS, and a unique ID
    vec sorted_mag;
    int sorted_marked_index[FINAL_ARRAY_SIZE][2];
    F(i,0,no_of_frames){
        //#Getting new array of size FRAME_SIZE + BACK_SECURE_FRAME
        int framesize_macro;
        //Incase adding frame_size is more than the array_size
        if( (marked_array[i] + FRAME_SIZE) > size)
            framesize_macro = -(marked_array[i]) + (size-1);
        else framesize_macro = FRAME_SIZE;
        
        vec new_array = corrOut.get(marked_array[i]-BACK_SECURE_FRAME, marked_array[i] + framesize_macro);
        int resIndexLen = (marked_array[i] + framesize_macro) - (marked_array[i]-BACK_SECURE_FRAME) + 1;
        if(resIndexLen<11)
            continue;
        
        //pICK OUT K elements,i.e, INDICES_FOR_PEAKS
        ivec resIndex = sort_index(new_array);
        resIndex = resIndex.get(resIndexLen-INDICES_FOR_PEAKS, resIndexLen-1);
        
        F(j,0,INDICES_FOR_PEAKS){
            sorted_mag = concat(sorted_mag, new_array[ resIndex[j] ]);  //Magnitude of the top-=peaks
            resIndex[j] = resIndex[j] + (marked_array[i]- BACK_SECURE_FRAME); //Adding @marked_array[], to get original index
            //CONCAT all top peak indexes, to sorted_marked_index
            sorted_marked_index[(i*INDICES_FOR_PEAKS) + j][0] = resIndex[j];
            sorted_marked_index[(i*INDICES_FOR_PEAKS) + j][1] = (i*INDICES_FOR_PEAKS) + j;
        }
    }
    FINAL_ARRAY_SIZE = sorted_mag.size();
    cout<<endl;
    F(i,0,FINAL_ARRAY_SIZE)
    {cout<<sorted_marked_index[i][0]<<" "<<sorted_marked_index[i][1]<<" "<<sorted_mag[i]<<endl;
    }
    
    //@sorted_mag_index : Containes indexes of the magnitude after sorting
    ivec sorted_mag_index;
    sorted_mag_index = sort_index(sorted_mag);
    
    int sort_type;          //Running for SORT5, SORT7, SORT10
    int sort_counter = 0;
    int cache[FINAL_ARRAY_SIZE][FINAL_ARRAY_SIZE];  //Cache to store already subtracted peaks for different sort_types
    F(i,0,FINAL_ARRAY_SIZE) //Initialize cache value with -1:
    F(j,0,FINAL_ARRAY_SIZE)
    cache[i][j] = -5;
    
    while(sort_counter<3){
        if(sort_counter == 0) sort_type = 5;
        else if(sort_counter == 1) sort_type = 7;
        else if(sort_counter == 2) sort_type = 10;
        else break;
        
        int maxIndices[sort_type*no_of_frames][2];  //Stores the sorted_indexes according to their magnitude of the @sort_type going on
        int max_size;
        max_size = sort_type*no_of_frames;
        cout<<"++";
        //After sorting magnitude index, putting the ORIGINAL index values of where that magnitude occurs
        int ct_maxInd = 0;
        F(i,0,FINAL_ARRAY_SIZE){
            //Choose 'i' such that top-x (5,7,10) peaks are fit into it. Like for top5, sorted_marked_index[(9-5):9], etc are chosen.
            if( sorted_mag_index[FINAL_ARRAY_SIZE - 1 - i]%INDICES_FOR_PEAKS >= (INDICES_FOR_PEAKS - sort_type) ){
                maxIndices[ct_maxInd][0] = sorted_marked_index[ sorted_mag_index[FINAL_ARRAY_SIZE - 1 - i] ][0];
                maxIndices[ct_maxInd][1] = sorted_marked_index[ sorted_mag_index[FINAL_ARRAY_SIZE - 1 - i] ][1];
                cout<<maxIndices[ct_maxInd][0]<<"^"<<maxIndices[ct_maxInd][1]<<" ";
                ct_maxInd++;
            }
        }
        
        
        //......short buffer
        //    ivec resIndex = sort_index(corrOut);
        //    int resIndexLen = resIndex.size();
        //    ivec maxIndices = resIndex.get(resIndexLen-INDICES_FOR_PEAKS, resIndexLen-1);
        //    int max_size = INDICES_FOR_PEAKS;
        int i, j;
        
        //##fOR nEW BUFFER
        //    vec resultPeaks;
        //    resultPeaks = "-1 -1 -1";
        //    for (i=0;i< INDICES_FOR_PEAKS; i++) {
        //        for (j=0;j < INDICES_FOR_PEAKS; j++) {
        //
        //            int peak1       = maxIndices[INDICES_FOR_PEAKS-1-i];
        //            int peak2       = maxIndices[INDICES_FOR_PEAKS-1-j];
        //            int absDistance = abs( peak1 - peak2 );
        //
        //            if (absDistance > MIN_DIS_BWT_PEAKS && absDistance < MAX_DIS_BWT_PEAKS) {
        //
        //                int FinalDistance = absDistance - MIN_DIS_BWT_PEAKS;
        //
        //                if ( FinalDistance % PEAK_FACTOR < 3) {
        //                    FinalDistance = FinalDistance - (FinalDistance % PEAK_FACTOR) + MIN_DIS_BWT_PEAKS;
        //
        //                }
        //
        //                if (FinalDistance % PEAK_FACTOR > 20) {
        //                    FinalDistance = FinalDistance + (PEAK_FACTOR - (FinalDistance % PEAK_FACTOR)) + MIN_DIS_BWT_PEAKS;
        //
        //                }
        //
        //                resultPeaks[0] = FinalDistance + MIN_DIS_BWT_PEAKS;
        //                if (peak1 > peak2) {
        //                    resultPeaks[1] = peak2;
        //                    resultPeaks[2] = peak1;
        //                }
        //                else {
        //                    resultPeaks[1] = peak1;
        //                    resultPeaks[2] = peak2;
        //                }
        //
        //            }
        //        }
        //    }
        //
        //    return resultPeaks;
        //
        //}
        
        //#FOR SHORTBUFFER
        int PEAK_detect_flag = 0;
        cout<<endl<<endl<<"-----------------------------------------";
        int peak1Selected;
        int peak2Selected;
        int temp_ct_peaks=0;
        for (i=0;i< max_size; i++) {
            peak1Selected = maxIndices[i][0];
            for (j=0; j<max_size; j++) {
                
                //            if (cache[maxIndices[i][1]][maxIndices[j][1]] >= -3 && cache[maxIndices[i][1]][maxIndices[j][1]]<=0) continue;
                //
                //
                //            if( cache[maxIndices[i][1]][maxIndices[j][1]] > 0 ){
                //                result_array[sort_counter] = cache[maxIndices[i][1]][maxIndices[j][1]];
                //                cout<<" cache%23 ";
                //                PEAK_detect_flag = 1;
                //                break;
                //            }
                //
                //            //If they occur in the same window
                //            if( maxIndices[i][1]/INDICES_FOR_PEAKS == maxIndices[j][1]/INDICES_FOR_PEAKS){
                //                cache[maxIndices[i][1]][maxIndices[j][1]] = -2;
                //                printf("NNN ij %d %d::%d ",i,j, -100);
                //                continue;
                //            }
                
                temp_ct_peaks++;
                peak2Selected = maxIndices[j][0];
                
                int absDistance = abs(peak1Selected - peak2Selected);
                printf("Abs ij %d %d::%d ",i,j,absDistance);
                if (absDistance > MIN_DIS_BWT_PEAKS && absDistance < 9800) {
                    int FinalDistance = (absDistance - MIN_DIS_BWT_PEAKS );
                    if (FinalDistance % PEAK_FACTOR == 0) {
                        cache[maxIndices[i][1]][maxIndices[j][1]] = FinalDistance + MIN_DIS_BWT_PEAKS;
                        result_array[sort_counter] = cache[maxIndices[i][1]][maxIndices[j][1]];
                        PEAK_detect_flag = 1;
                        break;
                    }
                    else cache[maxIndices[i][1]][maxIndices[j][1]] = -1; //If not a factor of 23, then -1.
                }
                else cache[maxIndices[i][1]][maxIndices[j][1]] = -3; //If not within range then -1.
            }
            if(PEAK_detect_flag == 1)
                break;
        }
        cout<<"\\Endofdistfunction";
        //IF no result has come out till now ,i.e, return_array didn't fill up ;then fill with -1
        if(PEAK_detect_flag == 0)
            result_array[sort_counter] = -1;
        cout<<result_array[sort_counter]<<" "<<temp_ct_peaks;
        
        sort_counter++;
    }//While loop
    
    cout<<endl;
    return result_array;
}
