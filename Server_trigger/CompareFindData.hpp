//
//  Finddata.hpp
//  AudioQueue
//
//  Created by Shivansh on 6/22/17.
//
//

#ifndef Finddata_hpp
#define Finddata_hpp

#import "itpp_headers.h"

using namespace itpp;
using namespace std;

class FindData {
    std::pair<int, int>findDataPtFromDistBwPeaks_sort(vec corrOut,const int &size, int top_peaks=5);
    std::pair<int, int>findDataPtFromDistBwPeaks_top20(vec corrOut,const int &size);
    
    
    float biggest_peak(vec corr_op, const int &size);
    vector<int> mark_every_frame(vec corr_op, const int &size);
    //@biggest_peak -> returns height of biggest peak
    //@mark_every_frame -> returns vector array of indexes where the peaks start
    
public:
    std::pair<int, int> apply(vec inputWave, int algorithm=1);
    int* findDataPtFromDistBwPeaks_v3(vec corrOut, int *result_array);
};


#endif /* Finddata_hpp */
