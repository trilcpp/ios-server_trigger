//
//  ShortBuffer.cpp
//  OLA-C
//
//  Created by Shivansh on 7/4/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//
#import "ViewController.h"

#include "CompareShortBuffer.hpp"
#include "CompareFinddata.hpp"

#define CHUNK_AFTER_SAMPLE 23000 //@We divide this by 2 for use in the last function


int ShortBuffer::putshortsinarray(string const& shorts, int &buffer_count, double result)
{
    if(flag==2) //If operations for calculating otp ARE still running
    {   //NSLog(@"String2");
        return -2;
    }
    
    c_buffer[buffer_count].clear();    //If our buffer is filled, clear it
    c_buffer[buffer_count] = shorts;   //write to our buffer
    
    //Enters everytime after trigger is detected
    if(flag==1)
    {
        char dataInterim[30];
        sprintf(dataInterim,"%f,", result);
        allTrigger.append(dataInterim);
        return WritetoStorage(buffer_count);
    }
    else if(result>70)
    {   allTrigger.clear();
        allpairDecoded.clear();
        allBufferString.clear();

        flag=1;
        cout<<"asdf:result :: "<<result<<endl;
        //When trigger is detected, start writing to @storage[]
        //To reduce comparison computation, not sending to Writetostorage()
        
        //Records previous 2 buffers, after the trigger is detected
        for(int j=mBUFFERS_BEFORE_TRIGGER; j>=1;j--)
        {
            int temp_buffer_count = (buffer_count-j < 0)? (STORAGE_VECTOR + buffer_count - j) : (buffer_count - j);
            storage[storagect++] = c_buffer[temp_buffer_count] ;
        }
        storage[storagect++] = c_buffer[buffer_count] ;
        //Add trigger
        char dataInterim[30];
        sprintf(dataInterim,"%f,", result);
        allTrigger.append(dataInterim);
        return -3;   //So that recording continues
    }
    else
        return 0;
}


int ShortBuffer::WritetoStorage(int buffer_count) //buffer_ct is the noraml STORAGE VECTOR COUNT - 4 in our case.
{
    cout<<"\nStorage count: "<<storagect<<endl;
    if(storagect == TOTAL_CHUNKS_HAVING_DATA + 1){
        storagect = 0;  //RESET
        flag=2;         //Made 2 so that nothing enters till all operations are completed
        return divideDataby();
    }
    
    else if(storagect == TOTAL_CHUNKS_HAVING_DATA) {
        for(int j=0; j<TOTAL_CHUNKS_HAVING_DATA ;j++)
        {   //cout<<"\n################Storage chunk :"<<j<<" "<<storage[j];
            allBufferString.append(storage[j]);
        }
        storagect++;
        return -5; //This would make the last WriteTostorage where calculations occur as async task
    }
    else {
        storage[storagect++]=c_buffer[buffer_count];
    }
    return -4; //NOT 0, so that Trigger calculation doesn't continue

}


//Dividing total number of chunks in storage[], and Finddata
int ShortBuffer::divideDataby()
{
    int division_sample_number = CHUNK_AFTER_SAMPLE; //DIVIDE_CHUNK_AFTER_SAMPLE/2 ;
    int sample_counter =0;
    
    string divided_storage[DIVIDE_TOTAL_CHUNKS_INTO];   //Array to store our final chunks, divided into respective parts.
    int count = 0; //Count for @divided_storage[], changes after every DIVIDE_CHUNK_AFTER_SAMPLE
    
    int ct_flag = 0; //Flag raised when the division is encountered. eg. to be divided after 1000 samples.
    
    //Divides the data
    //while (count!=DIVIDE_TOTAL_CHUNKS_INTO)
    F(i,0, TOTAL_CHUNKS_HAVING_DATA)
    {
        for(int j=0; storage[i][j]!='\0'; j++)
        {
            if(storage[i][j]==' ')  //Note: sample_counter remains the same till the space isn't encountered.
                sample_counter++;
            if(sample_counter >= division_sample_number) {
                    if(sample_counter!=division_sample_number && (sample_counter % (division_sample_number) == 0) && storage[i][j]==' ')
                    {ct_flag++;}
                 
                    if( ct_flag==1)
                        {   printf("\nCount %d & SAMPLE_count %d",count,sample_counter);
                            count++;
                            ct_flag=0;
                        }
                    if(count<DIVIDE_TOTAL_CHUNKS_INTO)
                    divided_storage[count].push_back(storage[i][j]) ;
                }
        }
    }
    cout<<"\nSample Total number : "<<sample_counter;
    cout<<"\nCount:"<<count;
#pragma mark - Comparison procedure
    FindData fdata;// Class name for findata funcitons
    std::pair<int,int> pair_result[DIVIDE_TOTAL_CHUNKS_INTO];
    cout<<"\n\n";
    int result_array[3];
    F(K, 0, TOTAL_ALGOS) {
        F(i,0 ,DIVIDE_TOTAL_CHUNKS_INTO)
        {   cout<<"#FR#"<<i;
            if(K+1!=7){
            pair_result[i] = fdata.apply( divided_storage[i], K+1);
            cout<<"\n\nFINAL RESULT::"<<pair_result[i].first<<"\n";
            }
            else {
            const int sz=3;
                fdata.findDataPtFromDistBwPeaks_v3(divided_storage[i], result_array);
            }
                
            char dataInterim[30];
            sprintf(dataInterim,"%d", pair_result[i].first);
            allpairDecoded.append(dataInterim);
            sprintf(dataInterim,"%d", pair_result[i].second);
            allpairDecoded.append(" --\\");
            allpairDecoded.append(dataInterim);
            if(i!=DIVIDE_TOTAL_CHUNKS_INTO - 1)
                allpairDecoded.append(",");
            
    //        if( ( i!=0 && (pair_result[i].first==pair_result[i-1].first) )|| ( (i-2)>=0 && (pair_result[i].first==pair_result[i-2].first)) )
    //        {   //Send : pair_result[i].first;
    //            cout<<"\n\nMajority Result::"<<pair_result[i].first<<endl;
    //            return pair_result[i].first;
    //            break;
    //        }
        }
        cout<<"========"<<K<<endl;

        if(K+1 == 2)
            allTrigger.append("sort5 ->");
        else if(K+1 == 3)
            allTrigger.append("sort6 ->");
        else if(K+1 == 4)
            allTrigger.append("sort7 ->");
        else if(K+1 == 5)
            allTrigger.append("sort8 ->");
        else if(K+1 == 6)
            allTrigger.append("sort10 ->");
        
        sendToView();
        
//NOTE : While uploading, all decoded results will be uploaded. As all pair decoded keeps remaking itself.
//        However trigger and buffer will only be sent onces
        allpairDecoded.clear();
        allTrigger.clear();
        allBufferString.clear();
    }
    //##flag To show that all operations for calculating OTP are done. And function can be entered again.
    flag=0;
    return pair_result[1 ].first;
}


void ShortBuffer::sendToView()
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ViewController *vc = [storyboard instantiateInitialViewController];

    NSString *BufferString = [[NSString alloc] initWithCString:allBufferString.c_str()
                                                      encoding:[NSString defaultCStringEncoding]];
    NSString *Pair = [[NSString alloc] initWithCString:allpairDecoded.c_str()
                                              encoding:[NSString defaultCStringEncoding]];
    NSString *Trigger = [[NSString alloc] initWithCString:allTrigger.c_str()
                                              encoding:[NSString defaultCStringEncoding]];

    cout<<allpairDecoded;
    cout<<endl<<allBufferString.length();
    [vc storeToShorts:BufferString : Pair: Trigger];

    [vc sendServer:nil];
    return;
}
