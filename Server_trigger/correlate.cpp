//
//  correlate.cpp
//  AudioQueue
//
//  Created by Shivansh on 6/22/17.
//
//

#include "correlate.hpp"
#include <itpp/itcomm.h>

vec Correlate::apply(vec wave1, vec wave2) {
    vec resCorr = xcorr( wave1, wave2, -1, "none" );
    return resCorr;
}
