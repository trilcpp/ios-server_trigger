//
//  AudioRecorderAppDelegate.m
//  AudioRecorder
//
//  Copyright Shivansh Jagga. All rights reserved.
//

#import "AudioRecorderAppDelegate.h"
#import "Trigerring.hpp"                //Can only declare in .mm file,else error
#import <CoreAudioKit/CoreAudioKit.h>
//#include "NewBuffer.hpp"             //Can only declare in .mm file
#include "CompareShortBuffer.hpp"
#import "ViewController.h"

//##Interface is like defining a class

//Global interface variable defined in .h
//@interface AudioRecorderAppDelegate : NSObject {
//    
//}
//@end


// Declare C callback functions
void AudioInputCallback(void * inUserData,  // Custom audio metadata
                        AudioQueueRef inAQ,
                        AudioQueueBufferRef inBuffer,
                        const AudioTimeStamp * inStartTime,
                        UInt32 inNumberPacketDescriptions,
                        const AudioStreamPacketDescription * inPacketDescs);

void AudioOutputCallback(void * inUserData,
                         AudioQueueRef outAQ,
                         AudioQueueBufferRef outBuffer);


@implementation AudioRecorderAppDelegate
@synthesize window;


//Initialization function
- init {
	if (self = [super init]) {
        
        //your initialization here
        NSLog(@"initialization");
        refToSelf = (__bridge void*)self; //NOTE : MADE FOR SAMPLES TO ENGINE function in AudioInputCallback
        
        
	}
	return self;
}


//##########################################################################################
//...........................SAMPLES........................................................
ShortBuffer strbuffer;            //OBJECT for Stringbuffer class
void *refToSelf;
int ct=0;
int check_result_flag=0;
//@ct -> Our own count for circular buffer for storing "shorts" in string, value goes upto @STORAGE_VECTOR
//@check_result_flag -> Gives back OTP result as well as other flags
//                   0 -> Continue to check trigger values for every @BUFFER_SIZE "shorts"
//                  -3 -> When trigger value is detected
//                  -4 -> When the next @TOTAL_CHUNKS_HAVING_DATA are being stored after detecting -3
//                  -5 -> When the final buffer is stored, so that ASYNCHRONOUS thread can be run when CORRELATION calculations will happen
//                  -2 -> When calculation for OTP is going on
//                  >0 -> OTP detected ; -1 -> Wrong OTP

- (void)feedSamplesToEngine:(UInt32)audioDataBytesCapacity audioData:(void *)audioData {
    int sampleCount = audioDataBytesCapacity / sizeof(SAMPLE_TYPE);
    SAMPLE_TYPE *samples = (SAMPLE_TYPE*)audioData;
    
    //SAMPLE_TYPE *sample_le = (SAMPLE_TYPE *)malloc(sizeof(SAMPLE_TYPE)*sampleCount );//for swapping endians
    
    std::string shorts;
    double power = pow(2,10);
    for(int i = 0; i < sampleCount; i++)
    {
        //EXTREME IMPORTANCE: the data recieved is in a format where the endianess,i.e, format of binary is in a different way
        //This is done to correct that error for "16BIT" binary number
        SAMPLE_TYPE sample_swap =  (0xff00 & (samples[i] << 8)) | (0x00ff & (samples[i] >> 8)) ; //Endianess issue
        
        char dataInterim[30];
        sprintf(dataInterim,"%f ", sample_swap/power); // normalize it.
        shorts.append(dataInterim);
    }
    
    //Enable the two lines for printing
    //NSLog(@"%s \n",result.c_str());
    //std::cout<<"\nRESULT: "<<result;
    //cout<<shorts<<"\n";
    
    //Checking trigger
    double trigger_result =1;
    cout<<"@"<<check_result_flag;
    if(check_result_flag != -5){
        Trigger tg; //initiated for later use
        trigger_result = tg.isFound(shorts );
        NSLog(@"\nTrigger for chunk no. %d : %f",++ct, trigger_result);
    }

    //Sending to paste to our own circular buffer
    ct = ct%(STORAGE_VECTOR);
    
    int send_to_corr;
    send_to_corr = strbuffer.putshortsinarray(shorts, ct, trigger_result);
    check_result_flag = send_to_corr;
    
    if(check_result_flag>0 || check_result_flag==-1)
    {
        //The below pointer must be created once in init
        ViewController * viewController = (ViewController *)self.window.rootViewController;
        //viewController.TextView.text = nil;
        if(check_result_flag==-1)
            viewController.TextView.text= [NSString stringWithFormat:@"OTP ! -v"];
        else
            viewController.TextView.text= [NSString stringWithFormat:@"%d", check_result_flag];
        
        [self stopRecording];
        [self startRecording];
        check_result_flag = 0;
        
    }
    
}

//............................TEST REMOTE I/O.............................................................................
//##########################################################################################

//.........................................................................................................
//##########################################################################################
// Takes a filled buffer and writes it to disk, "emptying" the buffer
void AudioInputCallback(void * inUserData, 
                        AudioQueueRef inAQ, 
                        AudioQueueBufferRef inBuffer, 
                        const AudioTimeStamp * inStartTime, 
                        UInt32 inNumberPacketDescriptions, 
                        const AudioStreamPacketDescription * inPacketDescs)
{
	RecordState * recordState = (RecordState*)inUserData;
    if (!recordState->recording)
    {
        printf("Not recording, returning\n");
    }

//     if (inNumberPacketDescriptions == 0 && recordState->dataFormat.mBytesPerPacket != 0)
//     {
//         inNumberPacketDescriptions = inBuffer->mAudioDataByteSize / recordState->dataFormat.mBytesPerPacket;
//     }

//######Used to write data to a file specified : Commented out as only buffers are needed
    
//   printf("Writing buffer %lld\n", recordState->currentPacket);
//    OSStatus status = AudioFileWritePackets(recordState->audioFile,
//                                            false,
//                                            inBuffer->mAudioDataByteSize,
//                                            inPacketDescs,
//                                            recordState->currentPacket,
//                                            &inNumberPacketDescriptions,
//                                            inBuffer->mAudioData);
//    
//    if (status == 0)
//    {
//        recordState->currentPacket += inNumberPacketDescriptions;
//    }

    AudioQueueEnqueueBuffer(recordState->queue, inBuffer, 0, NULL);

//######Searched for REMOTEIO API by tastypixel.com, TPCIrcularBuffer
//    AudioBufferList bufferList;
//    bufferList.mNumberBuffers = 20;
//    for(int j=0; j<bufferList.mNumberBuffers; j++)
//    {   bufferList.mBuffers[j].mNumberChannels = 1;
//        bufferList.mBuffers[j].mData = NULL;
//        bufferList.mBuffers[j].mDataByteSize = recordState->dataFormat.mFramesPerPacket * sizeof(SInt16) * 2;
//        
//        // Put audio into circular buffer
//        TPCircularBufferProduceBytes(&recordState->c_buffer, bufferList.mBuffers[j].mData, bufferList.mBuffers[j].mDataByteSize);
//    }
    
    //Do stuff with samples
    AudioRecorderAppDelegate *rec = (__bridge AudioRecorderAppDelegate *) refToSelf;
    [rec feedSamplesToEngine:inBuffer->mAudioDataBytesCapacity audioData:inBuffer->mAudioData];
    
}

//....................................... Fills an empty buffer with data and sends it to the speaker
void AudioOutputCallback(void * inUserData,
                         AudioQueueRef outAQ,
                         AudioQueueBufferRef outBuffer)
{
	PlayState* playState = (PlayState*)inUserData;	
    if(!playState->playing)
    {
        printf("Not playing, returning\n");
        return;
    }

	printf("Queuing buffer %lld for playback\n", playState->currentPacket);
    
    
    AudioStreamPacketDescription* packetDescs = NULL;
    
    
    UInt32 bytesRead;
    UInt32 numPackets = BUFFER_SIZE/2;
    //playState->mNumPacketsToRead ; //BUFFER_SIZE/2; because see sampleCount is 2048 in feedSamplesToEngine function
    OSStatus status;
    status = AudioFileReadPackets(playState->audioFile,
                                  false,
                                  &bytesRead,
                                  packetDescs,
                                  playState->currentPacket,
                                  &numPackets,
                                  outBuffer->mAudioData);
    cout<<numPackets;
    if (numPackets>0)
    {
        outBuffer->mAudioDataByteSize = bytesRead;
        status = AudioQueueEnqueueBuffer(outAQ, outBuffer, 0, NULL);
//        status = AudioQueueEnqueueBuffer(playState->queue,
//                                         outBuffer,
//                                         0,
//                                         packetDescs);
        
        playState->currentPacket += numPackets;
    }
    else
    {
        if (playState->playing)
        {
            AudioQueueStop(playState->queue, false);
            AudioFileClose(playState->audioFile);
            playState->playing = false;
        }
        
         AudioQueueFreeBuffer(playState->queue, outBuffer);
    }
            
}
//.##########################################################################################..

- (void)setupAudioFormat:(AudioStreamBasicDescription*)format 
{
    //SAMPLE_TYPE is short datatype
	format->mSampleRate = SAMPLE_RATE;
	format->mFormatID = kAudioFormatLinearPCM;
	format->mFramesPerPacket = 1;
	format->mChannelsPerFrame = 1;
	format->mBytesPerFrame = sizeof(SAMPLE_TYPE) * NUM_CHANNELS; //2;
	format->mBytesPerPacket = sizeof(SAMPLE_TYPE) * NUM_CHANNELS;    //2;
	format->mBitsPerChannel = 8 * sizeof(SAMPLE_TYPE);          //16;
	format->mReserved = 0;
    format->mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked | kLinearPCMFormatFlagIsBigEndian ;
    
    
}

- (void)recordPressed:(id)sender
{
    if (!playState.playing)
    {
        if (!recordState.recording)
        {
            printf("Starting recording\n");
            [self startRecording];
        }
        else
        {
            printf("Stopping recording\n");
            [self stopRecording];
        }
    }
    else
    {
        printf("Can't start recording, currently playing\n");
    }
}

- (void)playPressed:(id)sender
{
    if (!recordState.recording)
    {
        if (!playState.playing)
        {
            printf("Starting playback\n");
            [self startPlayback];
        }
        else
        {
            printf("Stopping playback\n");
            [self stopPlayback];
        }
    }
}

//#############################

- (void)startRecording
{
    [self setupAudioFormat:&recordState.dataFormat];
    NSLog(@"Sample : %f",recordState.dataFormat.mSampleRate);
    
    recordState.currentPacket = 0;
	
    
    OSStatus status;
    status = AudioQueueNewInput(&recordState.dataFormat,
                                AudioInputCallback,
                                &recordState,
                                CFRunLoopGetCurrent(),
                                kCFRunLoopCommonModes,
                                0,
                                &recordState.queue);
    if (status == 0)
    {
        // Prime recording buffers with empty data
        for (int i = 0; i < NUM_BUFFERS; i++)
        {
            AudioQueueAllocateBuffer(recordState.queue, BUFFER_SIZE , &recordState.buffers[i]);
            AudioQueueEnqueueBuffer (recordState.queue, recordState.buffers[i], 0, NULL);
        }
        
        status = AudioFileCreateWithURL(fileURL, //if returns -50, then fileURL= " "should have path
                                        kAudioFileAIFFType,
                                        &recordState.dataFormat,
                                        kAudioFileFlags_EraseFile,
                                        &recordState.audioFile);
        
        if (status == 0)
        {   NSLog(@"Entered");
            recordState.recording = true;        
            status = AudioQueueStart(recordState.queue, NULL);
            if (status == 0)
            {
                labelStatus.text = @"Recording";
            }
        }
    }
    
    if (status != 0)
    {
        NSLog(@"Statys : %d",status);
        [self stopRecording];
        labelStatus.text = @"Record Failed";
    }
    
    
}

- (void)stopRecording
{
    recordState.recording = false;
    
    AudioQueueStop(recordState.queue, true);
    for(int i = 0; i < NUM_BUFFERS; i++)
    {
        AudioQueueFreeBuffer(recordState.queue, recordState.buffers[i]);
    }
    
    AudioQueueDispose(recordState.queue, true);
    AudioFileClose(recordState.audioFile);
    labelStatus.text = @"Idle";
}

//#####################################

- (void)startPlayback
{
    
    playState.currentPacket = 0;
    
    [self setupAudioFormat:&playState.dataFormat];
    
    OSStatus status;
    
    //..trill
    status = AudioFileOpenURL(fileURL, kAudioFileReadPermission, kAudioFileAIFFType, &playState.audioFile);
    
    if (status == 0)
    {
        status = AudioQueueNewOutput(&playState.dataFormat,
                                     AudioOutputCallback,
                                     &playState,
                                     CFRunLoopGetCurrent(),
                                     kCFRunLoopCommonModes,
                                     0,
                                     &playState.queue);
        
        if (status == 0)
        {
            // Allocate and prime playback buffers
            playState.playing = true;
            for (int i = 0; i < NUM_BUFFERS && playState.playing; i++)
            {
                
                AudioQueueAllocateBuffer(playState.queue, BUFFER_SIZE, &playState.buffers[i]);
                AudioOutputCallback(&playState, playState.queue, playState.buffers[i]);
            }

            status = AudioQueueStart(playState.queue, NULL);
            if (status == 0)
            {
                labelStatus.text = @"Playing";
            }
        }        
    }
    
    if (status != 0)
    {
        [self stopPlayback];
        labelStatus.text = @"Play failed";
    }
}

- (void)stopPlayback
{
    playState.playing = false;
    
    for(int i = 0; i < NUM_BUFFERS; i++)
    {
        AudioQueueFreeBuffer(playState.queue, playState.buffers[i]);
    }
        
    AudioQueueDispose(playState.queue, true);
    AudioFileClose(playState.audioFile);
}

//When ARC is yes
//- (void)dealloc
//{
//    CFRelease(fileURL);
//    [labelStatus release];
//    [buttonRecord release];
//    [buttonPlay release];
//    [window release];
//    [super dealloc];
//
//}

//.......................................................................................


- (BOOL)getFilename:(char*)buffer maxLenth:(int)maxBufferLength
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, 
            NSUserDomainMask, YES); 
    NSString* docDir = [paths objectAtIndex:0];
    
    NSString* file = [docDir stringByAppendingString:@"/recording.aif]"];
    return [file getCString:buffer maxLength:maxBufferLength encoding:NSUTF8StringEncoding];
}




- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    /*UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *vc = [storyboard instantiateInitialViewController];
    
    // Set root view controller and make windows visible
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];*/
    
    //ACCESS MAIN.STORYBOARD ELEMENTS
//    ViewController * viewController = (ViewController *)self.window.rootViewController;
//    NSLog(@"This is my text: %@", viewController.TextView.text);
    
    //copy trill ..Get audio file page
    char path[256];
    [self getFilename:path maxLenth:sizeof path];
    fileURL = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path, strlen(path), false);
    
    // Init state variables
    playState.playing = false;
    recordState.recording = false;
    

    
    ///....................................THIS WAS previously inputted in the audioservices file
    // Create window
   /* self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    
    // Create status label
    labelStatus = [[UILabel alloc] initWithFrame: CGRectMake(10, 50, 300, 30)];
    labelStatus.textAlignment = UITextAlignmentCenter;
    labelStatus.numberOfLines = 1;
    labelStatus.text = @"Idle";
    
    // Create Record button
    buttonRecord = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [buttonRecord setTitle:@"Record" forState:UIControlStateNormal];
    [buttonRecord addTarget:self action:@selector(recordPressed:)
           forControlEvents:UIControlEventTouchUpInside];
    //buttonRecord.center = CGPointMake(window.center.x, 200);
    buttonRecord.frame = CGRectMake(6.0, 120.0, 140, 35);
    buttonRecord.backgroundColor = [UIColor clearColor];
    
    // Create Play button
    buttonPlay = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [buttonPlay setTitle:@"Play" forState:UIControlStateNormal];
    [buttonPlay addTarget:self action:@selector(playPressed:)
         forControlEvents:UIControlEventTouchUpInside];
    //buttonPlay.center = CGPointMake(window.center.x, 150);
    buttonPlay.frame = CGRectMake(161.0, 120.0, 140, 35);
    buttonPlay.backgroundColor = [UIColor clearColor];
    
    // Get audio file page
    char path[256];
    [self getFilename:path maxLenth:sizeof path];
    fileURL = CFURLCreateFromFileSystemRepresentation(NULL, (UInt8*)path, strlen(path), false);
    
    // Init state variables
    playState.playing = false;
    recordState.recording = false;
    
    // Add the controls to the window
    [window addSubview:labelStatus];
    [window addSubview:buttonRecord];
    [window addSubview:buttonPlay];
    
    [window makeKeyAndVisible];
    */
//////...............................
    
    
    return YES;
}

@end
